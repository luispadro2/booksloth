module.exports = function(pool,user) {
	const sprintf = require("sprintf-js").sprintf
	const request = require('request');
	const xml2js = require('xml2js');


	function insertBooks(books) {

	var parser = new xml2js.Parser();


	books.forEach((book, i) => {
			var author = book.Author.trim().replace(/[\\"']/g, '\\$&').split(","); //[lastName, firstname]
			var insertAuthor = sprintf('INSERT INTO author(first_name, last_name) SELECT * FROM (SELECT "%s" as first_name , "%s" as last_name) AS tmp WHERE NOT EXISTS (SELECT * FROM author WHERE first_name = "%s" AND last_name = "%s" ) LIMIT 1; ', author[1], author[0], author[1], author[0]);
			
			//var insertBook = sprintf('INSERT INTO book(isbn, title) VALUES ("%s", "%s");', book.ean, book.Title);
			var authorId = sprintf('SELECT * FROM author WHERE first_name = "%s" AND last_name = "%s"', author[1], author[0])
			var getId = sprintf('SELECT id FROM book WHERE isbn = "%s";', book.ean);
			goodReadAPI(book.ean)
			
			var insertBook = sprintf('INSERT INTO book(isbn, title) SELECT * FROM (SELECT "%s" as isbn , "%s" as title) AS tmp WHERE NOT EXISTS (SELECT isbn FROM book WHERE isbn = "%s" ) LIMIT 1; ', book.ean, book.Title,book.ean);
			pool.query(insertBook).then(() => {

				pool.query(getId).then((result) => {
					pool.query(insertAuthor).then(() => {
						pool.query(authorId).then((idRes) =>{
							var bookAuthor = sprintf('INSERT INTO book_author(book_id, author_id) VALUES ("%s", "%s");', result[0].id, idRes[0].id)
							pool.query(bookAuthor).then((res) => {//console.log(res)
							})
							.catch((err) => {
								console.error(err)
							})

						})
					}).catch((err) => {
								console.log('isbn: '+book.ean)
								console.error(err)
							})
					//console.log(JSON.stringify(result))
					book.slothGenres.forEach((genre, j) => {
						if (genre != "") {
							var genreQuerie = sprintf('INSERT INTO book_genre(book_id, genre_id) VALUES ("%s", "%s");', result[0].id, genre);
							pool.query(genreQuerie).then((res) =>{ //console.log(res)
							})
							.catch((err) => {
								console.error(err)
							})
						}
					})
				})


			}).catch((err) => {
								console.error(err)
							})
		})
	}

	function goodReadAPI(key){
    var parser = new xml2js.Parser();

    request('https://www.goodreads.com/search/index.xml?key='+user.gooReadsKey+'&q='+ key, function(error, response, body) {
        parser.parseString(body, function(err, result) {
            console.log("Good read:\n"+JSON.stringify(result));
        });

    });
	}

	return {
		insertBooks
	}
}