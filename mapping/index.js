module.exports = function(fse) {

	const genreMap = [{
		"genres": ["Action & Adventure", "Interactive Adventures", "Adventurers & Explorers"],
		"genre_id": 1
	}, {
		"genres": ["Biographical", "Biography & Autobiography", "Cultural Heritage", "Diaries & Journals", "\"Editors', ' Journalists', ' Publishers\"", "Personal Memoirs", "Adventurers & Explorers", "Presidents & Heads Of State", "\"Artists', ' Architects', ' Photographers\"", "Composers & Musicians", "Criminals & Outlaws"],
		"genre_id": 2
	}, {
		"genres": ["Business & Economics", "Business & Financial", "Business Writing", "\"Business', ' Careers', ' Occupations\"", "Careers", "Decision-Making & Problem Solving", "Development", "E-Commerce", "Economics", "Entrepreneurship", "Finance", "Human Resources & Personnel Management", "Industries", "Leadership", "Management", "Marketing", "Negotiating", "Production & Operations Management", "Project Management", "Sales & Selling", "Skills", "Small Business", "Strategic Planning", "Training", "Economic History", "Personal Finance", "Personal Success", "Professional Development"],
		"genre_id": 3
	}, {
		"genres": ["Juvenile Fiction", "Juvenile Nonfiction"],
		"genre_id": 4
	}, {
		"genres": ["Contemporary Women"],
		"genre_id": 5
	}, {
		"genres": ["Crime", "Noir", "True Crime", "Criminals & Outlaws", "Crime & Mystery"],
		"genre_id": 6
	}, {
		"genres": ["Drama", "Shakespeare"],
		"genre_id": 7
	}, {
		"genres": ["Comics & Graphic Novels", "Manga"],
		"genre_id": 8
	}, {
		"genres": ["Fantasy", "Fantasy & Magic", "Ghost Stories", "Vampires", "Fairy Tales & Folklore", "Folklore & Mythology", "\"Legends', ' Myths', ' Fables\"", "Monsters"],
		"genre_id": 9
	}, {
		"genres": ["Fiction", "Occult & Supernatural", "Dystopian", "Fairy Tales & Folklore", "Folklore & Mythology", "Juvenile Fiction", "\"Legends', ' Myths', ' Fables\"", "Monsters", "Superheroes", "Young Adult Fiction", "Vampires"],
		"genre_id": 10
	}, {
		"genres": ["Alternative History", "Historical"],
		"genre_id": 11
	}, {
		"genres": ["Ancient", "Historical Geography", "History", "History & Criticism", "History & Surveys", "History & Theory", "Holocaust", "Legal History", "Medieval", "Native American", "Renaissance", "Revolutionary", "Social History", "Historical", "Native Americans", "Presidents & Heads Of State", "Economic History"],
		"genre_id": 12
	}, {
		"genres": ["Beverages", "Cooking", "Cooking & Food", "Courses & Dishes", "Flowers", "Garden Design", "Gardening", "House & Home", "Plants"],
		"genre_id": 13
	}, {
		"genres": ["Horror"],
		"genre_id": 14
	}, {
		"genres": ["Humor", "Humorous", "Humorous Stories"],
		"genre_id": 15
	}, {
		"genres": ["Juvenile Fiction", "Juvenile Nonfiction"],
		"genre_id": 16
	}, {
		"genres": ["Mysteries & Detective Stories", "Mystery & Detective", "Thrillers", "Thrillers & Suspense", "Crime & Mystery"],
		"genre_id": 17
	}, {
		"genres": ["Animals", "Antiques & Collectibles", "Architecture", "Education", "Family & Relationships", "Foreign Language Study", "Games & Activities", "Health & Fitness", "Juvenile Nonfiction", "Language Arts & Disciplines", "Law", "Marriage & Long Term Relationships", "Nature", "Nonfiction", "Pets", "Philosophy", "Political Science", "Psychology", "Sports", "Sports & Recreation", "Transportation", "Travel", "Young Adult Nonfiction"],
		"genre_id": 18
	}, {
		"genres": ["Poetry", "Stories In Verse"],
		"genre_id": 19
	}, {
		"genres": ["Erotica", "Love & Romance", "Romance"],
		"genre_id": 20
	}, {
		"genres": ["Amish & Mennonite", "Atheism", "Biblical Commentary", "Biblical Criticism & Interpretation", "Biblical Meditations", "Biblical Studies", "\"Body', ' Mind & Spirit\"", "Buddhism", "Christian", "Christian Church", "Christian Theology", "Christianity", "Dreams", "Faith", "Islam", "Judaism", "Parapsychology", "Philosophy", "Psychology Of Religion", "Religion", "Religious", "Sermons", "Spiritual", "Spiritualism", "Spirituality", "Theology", "Religion & Science"],
		"genre_id": 21
	}, {
		"genres": ["Robots", "Science Fiction"],
		"genre_id": 22
	}, {
		"genres": ["Anthropology", "Archaeology", "Astronomy", "Religion & Science", "Biotechnology", "Chemical & Biochemical", "Chemistry", "Cognitive Science", "Computer Science", "Criminology", "Earth Sciences", "Ecology", "Environmental", "Environmental Science", "Genetics", "Global Warming & Climate Change", "Gravity", "Medical", "Medical", "Natural History", "Paleontology", "Philosophy", "Philosophy & Social Aspects", "Physics", "Programming", "Programming Languages", "Psychology", "Science", "Science & Nature", "Science & Technology", "Social Science", "Software Development & Engineering", "Technology", "Technology & Engineering", "Web", "Astrology", "Computers"],
		"genre_id": 23
	}, {
		"genres": ["Alternative Therapies", "Health & Fitness", "Health & Healing", "Healthy Living", "Inspiration & Personal Growth", "Mental Health", "Motivational & Inspirational", "Personal Growth", "Self-Help", "Twelve-Step Programs", "Motivational", "Personal Finance", "Personal Success", "Professional Developmentp[o;]"],
		"genre_id": 24
	}, {
		"genres": ["Thrillers & Suspense"],
		"genre_id": 25
	}, {
		"genres": ["Coming Of Age", "Young Adult Fiction", "Young Adult Nonfiction"],
		"genre_id": 26
	}, {
		"genres": ["Art", "Art & Architecture", "Body Art & Tattooing", "Book Printing & Binding", "Crafts & Hobbies", "Decorating", "Design", "Fashion & Accessories", "Graphic Arts", "Individual Artists", "Interior Design", "Mixed Media", "Music", "Painting", "Performing Arts", "Photography", "Photojournalism", "Popular Culture", "Techniques", "Theater", "\"Artists', ' Architects', ' Photographers\"", "Composers & Musicians", "Entertainment & Performing Arts"],
		"genre_id": 29
	}, {
		"genres": ["Ancient & Classical", "Classics", "Shakespeare"],
		"genre_id": 30
	}, {
		"genres": ["Literary", "Literary Collections", "Literary Criticism", "Shakespeare"],
		"genre_id": 31
	}]

	function mapGenres(path) {
		return getBooksJson(path).then(jBooks => {
			jBooks.forEach((book, i) => {
				let genres = book["Bisac Category Description"].split('/').map(s => s.trim())
				jBooks[i].slothGenres = findEquivalent(genres);
			})
			return jBooks
		})


	}
	async function getBooksJson(path) {
		await fse.stat(path)
		return new Promise((resolve, reject) => {
			resolve(JSON.parse(fse.readFileSync(path)))
		});

	}

	function findEquivalent(genres) {
		let matches = genres.map(genre => {
			let match = genreMap.find(x => x.genres.find(y => y == genre))
			return (match ? match.genre_id : "")
		})
		return [...new Set(matches)] //returning unique values for genres
	}

	return {
		genreMap,
		mapGenres
	}
}