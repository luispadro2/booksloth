const express = require('express')
const mysql = require('promise-mysql')
const chromes = require('chromedriver');
var webdriver = require('selenium-webdriver');
var bodyParser = require('body-parser');
const app = express()
const PORT = process.env.PORT || 3000;
const path = require('path')
const fs = require('fs');
const fse = require('fs-extra');
const command = require('selenium-webdriver/lib/command');
const credentials = {
	host: 'us-cdbr-iron-east-01.cleardb.net',
	user: 'bfa04d61c8724c',
	password: 'b4e1ed23',
	database: 'heroku_ef93bae512855f9'
}
pool = mysql.createPool(credentials);
/*{
	host: 'us-cdbr-iron-east-01.cleardb.net',
	user: 'ba0a84ca268113',
	password: '29db18f3',
	database: 'heroku_adbdd99b8eb5aa7'
}*/

const user ={
	username: "luis.padro2@upr.edu",
	password: "siulnav1",
	goodReadsKey: "aPXYANK8zv1ezMGqG5PWA"
}

const scraper = require('./scrape')(webdriver,fs)
const map = require('./mapping')(fse)

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

//Opertional order for scrapping website
//scrape -> parse -> upload -> insert
app.get('/api/scrape/:catalogID?', function(req, res) {
	
	let catId = req.params.catalogID || "4294104"

	scraper.getBooks(catId,req,res,user)
		.then(() => parse())
		.then(() => upload())
		.then(() => insert())
		.then(() => {console.log('done succesfully')})
		.catch((error) => {
			console.log('Something went wrong')
			console.error(error)});
});


app.get('/api/', (req, res) => {
	mysql.createConnection(credentials)
		.then(function(conn) {
			let result = conn.query('SHOW TABLES');
			conn.end();
			return result;
		}).then(function(rows) {
			res.send(JSON.stringify(rows))
		}).catch((error) => {
			console.error(error)
		});

})

app.listen(PORT, () => console.log('Example app listening on port 3000!'))


app.get('/api/file', (req, res) => {
	var log = require('log-to-file');
	node_xj = require("xls-to-json");
	node_xj({
		input: "./download/OrderExport.xls", // input xls
		output: "./books/output.json", // output json
		sheet: "Order" // specific sheetname
	}, function(err, result) {
		if (err) {
			console.error(err);
			res.send(JSON.stringify(err))
		} else {
			console.log(result)
			res.send("hello")
		}
	});
})

app.get('/api/upload', (req, res) => {
	let map = require('./mapping')(fse)
	map.mapGenres('./books/output.json')
	.then(books=>{
		res.send("done:" + JSON.stringify(books))
		fs.writeFile('booksGenred.json', JSON.stringify(books), 'utf8',()=>{console.log("done")});
	})
})

app.get('/api/insert', (req, res) => {
	let querie = require('./queries')(pool,user)
	querie.insertBooks(JSON.parse(fse.readFileSync('booksGenred.json')));
	res.send("done:")
})

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});


function upload(){
	return new Promise((resolve,reject) =>{
		try{
		map.mapGenres('./books/output.json')
		.then(books=>{
			fs.writeFile('booksGenred.json', JSON.stringify(books), 'utf8',()=>{console.log("done")});
			})
		resolve()
		}
		catch(error){
			reject(error)
		}
	})
}

function insert(){
	
	return new Promise((resolve,reject) =>{
		try{
			console.log('insert')
			let querie = require('./queries')(pool,user)
			querie.insertBooks(JSON.parse(fse.readFileSync('booksGenred.json')));
			resolve()
		}
		catch(error){
			reject(error)
		}
	})
}

function parse(){
	return new Promise((resolve,reject) =>{
		try{
			var log = require('log-to-file');
			node_xj = require("xls-to-json");
			node_xj({
				input: "./scrape/download/OrderExport.xls", // input xls
				output: "./books/output.json", // output json
				sheet: "Order" // specific sheetname
			}, function(err, result) {
				if (err) {
					reject(err)
				} else {
					fse.unlink('./scrape/download/OrderExport.xls').then(() =>{
						console.log('deleted') 
						resolve()})
					console.log(result)
				}
			});
		}
		catch(error){
			reject(error)
		}
	})
}
