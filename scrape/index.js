module.exports = function(webdriver,fs) {
	const path = require('path')

	async function getBooks(catId,req,res,user) {
		var obj = {};
		const {
			Builder,
			By,
			Key,
			until
		} = await require('selenium-webdriver');

		let chrome = await require('selenium-webdriver/chrome');
			let driver = {}
			let err = {}
			const width = 640;
			const height = 480;
	  return new  Promise(async (resolve,reject)=>{
		try {
			driver = await new Builder()
				.withCapabilities(webdriver.Capabilities.chrome())
				.forBrowser('chrome')
				.setChromeOptions(new chrome.Options() /*.headless()*/ .setUserPreferences({
					'download.prompt_for_download': false,
					'download.default_directory': __dirname + "/download"
				}))
				.build();

			console.log("Initializing Browser Session")
			await driver.get('https://www.edelweiss.plus/#catalogID=' + catId + '&page=1');

			console.log("Getting Calagog: " + catId)
			await driver.wait(until.elementLocated(By.className('listItem_0')), 10000)
			obj.title = await driver.findElement(By.id('userProfile')).getText().catch(async function(error) {
				await driver.navigate().to("https://www.edelweiss.plus/")
				console.log("Account not found Login in as " + "luis.padro2@upr.edu")
				await driver.wait(until.elementLocated(By.id('welcome_logIn')), 10000)
				await driver.executeScript("changeWelcomeArea('login');")
				await driver.findElement(By.id('txtPassword')).sendKeys(user.password)
				await driver.findElement(By.id('txtUserID')).sendKeys(user.username)

				return await driver.findElement(By.id('loginButton')).click()
			})
			await driver.sleep(3000)
			await driver.navigate().to('https://www.edelweiss.plus/#catalogID=' + catId + '&page=1')
			await driver.wait(until.elementLocated(By.className('listItem_0')), 10000)
			obj.title = await driver.findElement(By.id('userProfile')).getText().catch();

			await driver.executeScript("selectAllItems();\r\n(function createAndExportForm(n,t,i,r,u,f,e,o,s,h){\r\n    $.url=\"\/order\/export\/POSExport.aspx\";\r\n    var c=\"<form id='exportForm' target='_blank' action='\"+$.url+\"' method='POST'><input type='hidden' name='exportType' value='\"+\r\n            n+\"'><input type='hidden' name='reportname' value='\"+t+\"'><input type='hidden' name='skuList' value='\"+i\r\n            +\"'><input type='hidden' name='orderID' value='\"+r+\"'><input type='hidden' name='analysisCacheKey' value='\"+\r\n            u+\"'><input type='hidden' name='stockAnalysisClass' value='\"+f+\"'><input type='hidden' name='filtersCacheKey' value='\"+\r\n            e+\"'><input type='hidden' name='doSort' value='false'><input type='hidden' name='inventory' value='\"+h+\"'>\";\r\n    _.isNil(o)||(c+=\"<input type='hidden' name='catalogID' value='\"+o+\"'>\");\r\n    _.isNil(s)||(c+=\"<input type='hidden' name='mailingID' value='\"+s+\"'>\");\r\n    c=c+\"<\\\/form>\";\r\n    $(\"body\").append(\"<div id=\\\"frameWrapper\\\"><\/div>\")\r\n    $(\"#frameWrapper\").html(\"\");\r\n    $(\"#frameWrapper\").append(c);\r\n    $(\"#exportForm\").submit()\r\n})(10,\"hey\",window.getSelectedItems(),0,\"\",0,undefined,0,0,false)")
			await driver.sleep(3000)
			console.log("Finished Scraping")

		} catch (error) {
			console.error(error)
			err.catch = error;
		} finally {
			try {
				console.log("finally here")
				await driver.quit();
				obj.status = "Done"
				obj.err = err;


				function fromDir(startPath, filter) {

					if (!fs.existsSync(startPath)) {
						console.log("no dir ", startPath);
						return;
					}

					var files = fs.readdirSync(startPath);
					for (var i = 0; i < files.length; i++) {
						var filename = path.join(startPath, files[i]);
						var stat = fs.lstatSync(filename);
						if (stat.isDirectory()) {
							fromDir(filename, filter); //recurse
						} else if (filename.indexOf(filter) >= 0) {
							console.log('-- found: ', filename);
						};
					};
				};

				fromDir(__dirname +'/download', '.xls');
				res.status(200).send(JSON.stringify(obj));
				resolve()
			} catch (error) {
				console.error(error)
				err.finally = error
				res.status(200).send(JSON.stringify(err));
				reject(error)
			}
		}
		})
	}



	return {
		getBooks
	}
}